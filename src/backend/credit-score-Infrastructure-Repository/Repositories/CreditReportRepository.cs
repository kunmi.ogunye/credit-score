﻿using credit_score_Application.Interfaces;
using credit_score_Domain.Models;
using credit_score_Infrastructure_Repository.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace credit_score_Infrastructure_Repository.Repositories
{
    internal sealed class CreditReportRepository : RepositoryBase<CreditReport>, ICreditReportRepository
    {
        public CreditReportRepository(RepositoryContext dbContext) : base(dbContext)
        {
        }

        public void CreateCreditReport(CreditReport creditReport)
        {
            Create(creditReport);
        }

        public void DeleteCreditReport(CreditReport creditReport)
        {
            Delete(creditReport);
        }

        public async Task<IEnumerable<CreditReport>> GetAllCreditReports(bool trackChanges) => await GetAll(trackChanges)
            .OrderBy(x => x.Id)
            .ToListAsync();


        public async Task<CreditReport> GetCreditReportById(int id, bool trackChanges)
        {
            return await GetByCondition(x => x.Id.Equals(id), trackChanges).FirstOrDefaultAsync();
        }        

    }
}
