﻿using credit_score_Application.BaseContracts;
using credit_score_Infrastructure_Repository.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace credit_score_Infrastructure_Repository
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected RepositoryContext ApplicationDbContext;

        public RepositoryBase(RepositoryContext dbContext)
        {
            ApplicationDbContext = dbContext;
        }

        public void Create(T entity)
        {
            Create(entity);
        }

        public void Delete(T entity)
        {
            Delete(entity);
        }

        public IQueryable<T> GetAll(bool trackChange) => !trackChange ?
                ApplicationDbContext.Set<T>()
                .AsNoTracking() :
                ApplicationDbContext.Set<T>();

        public IQueryable<T> GetByCondition(Expression<Func<T, bool>> condition, bool trackChanges) => !trackChanges ?
                ApplicationDbContext.Set<T>()
                .Where(condition)
                .AsNoTracking() :
                ApplicationDbContext.Set<T>()
                .Where(condition);

        public void Update(T entity)
        {
            Update(entity);
        }
    }
}
