﻿using credit_score_Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace credit_score_Infrastructure_Repository.Contexts
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions<RepositoryContext> options): base(options)
        {
            
        }

        public DbSet<CreditReport>? CreditReports { get; set; }
        public DbSet<User>? Users { get; set; }
        public DbSet<LoanApplication>? LoanApplications { get; set; }
        public DbSet<LoanApproval>? LoanApprovals { get; set; }
        public DbSet<LoanEligibility>? LoanEligibilities { get; set; }
    }
}
