﻿using credit_score_Application.BaseContracts;
using credit_score_Application.Interfaces;
using credit_score_Infrastructure_Repository.Contexts;
using credit_score_Infrastructure_Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace credit_score_Infrastructure_Repository
{
    public sealed class RepositoryManager : IRepositoryManager
    {
        private readonly RepositoryContext _dbContext;
        private readonly Lazy<ICreditReportRepository> _creditReportRepository;

        public RepositoryManager(RepositoryContext dbContext)
        {
            _dbContext = dbContext; 
            _creditReportRepository = new Lazy<ICreditReportRepository>(() => new CreditReportRepository(dbContext));
        }
        public ICreditReportRepository CreditReport => _creditReportRepository.Value;

        public async Task SaveAsync() => await _dbContext.SaveChangesAsync();
    }
}
