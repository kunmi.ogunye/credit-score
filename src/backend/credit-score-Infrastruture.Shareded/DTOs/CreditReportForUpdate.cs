﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace credit_score_Infrastruture.Shareded.DTOs
{
    public class CreditReportForUpdate
    {
        public int Id { get; set; }
        public int user_id { get; set; }
        public string? credit_history { get; set; }
        public decimal debts { get; set; }
    }
}
