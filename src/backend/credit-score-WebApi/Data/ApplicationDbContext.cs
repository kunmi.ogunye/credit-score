﻿using credit_score_WebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace credit_score_WebApi.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        public DbSet<User>? Users { get; set; }
        public DbSet<LoanApplication>? LoanApplications { get; set; }
        public DbSet<LoanApproval>? LoanApproval { get; set;}
        public DbSet<LoanEligibility>? LoanEligibilities { get; set; }
        public DbSet<CreditReport>? CreditReports { get; set; }
    }
}
