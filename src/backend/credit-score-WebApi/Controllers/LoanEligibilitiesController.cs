﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using credit_score_WebApi.Data;
using credit_score_WebApi.Models;

namespace credit_score_WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoanEligibilitiesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public LoanEligibilitiesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/LoanEligibilities
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LoanEligibility>>> GetLoanEligibilities()
        {
            return await _context.LoanEligibilities.ToListAsync();
        }

        // GET: api/LoanEligibilities/5
        [HttpGet("{id}")]
        public async Task<ActionResult<LoanEligibility>> GetLoanEligibility(int id)
        {
            var loanEligibility = await _context.LoanEligibilities.FindAsync(id);

            if (loanEligibility == null)
            {
                return NotFound();
            }

            return loanEligibility;
        }

        // PUT: api/LoanEligibilities/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLoanEligibility(int id, LoanEligibility loanEligibility)
        {
            if (id != loanEligibility.Id)
            {
                return BadRequest();
            }

            _context.Entry(loanEligibility).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LoanEligibilityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LoanEligibilities
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<LoanEligibility>> PostLoanEligibility(LoanEligibility loanEligibility)
        {
            _context.LoanEligibilities.Add(loanEligibility);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLoanEligibility", new { id = loanEligibility.Id }, loanEligibility);
        }

        // DELETE: api/LoanEligibilities/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLoanEligibility(int id)
        {
            var loanEligibility = await _context.LoanEligibilities.FindAsync(id);
            if (loanEligibility == null)
            {
                return NotFound();
            }

            _context.LoanEligibilities.Remove(loanEligibility);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool LoanEligibilityExists(int id)
        {
            return _context.LoanEligibilities.Any(e => e.Id == id);
        }
    }
}
