﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using credit_score_WebApi.Data;
using credit_score_WebApi.Models;

namespace credit_score_WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoanApprovalsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public LoanApprovalsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/LoanApprovals
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LoanApproval>>> GetLoanApproval()
        {
            return await _context.LoanApproval.ToListAsync();
        }

        // GET: api/LoanApprovals/5
        [HttpGet("{id}")]
        public async Task<ActionResult<LoanApproval>> GetLoanApproval(int id)
        {
            var loanApproval = await _context.LoanApproval.FindAsync(id);

            if (loanApproval == null)
            {
                return NotFound();
            }

            return loanApproval;
        }

        // PUT: api/LoanApprovals/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLoanApproval(int id, LoanApproval loanApproval)
        {
            if (id != loanApproval.loan_approval_id)
            {
                return BadRequest();
            }

            _context.Entry(loanApproval).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LoanApprovalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LoanApprovals
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<LoanApproval>> PostLoanApproval(LoanApproval loanApproval)
        {
            _context.LoanApproval.Add(loanApproval);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLoanApproval", new { id = loanApproval.loan_approval_id }, loanApproval);
        }

        // DELETE: api/LoanApprovals/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLoanApproval(int id)
        {
            var loanApproval = await _context.LoanApproval.FindAsync(id);
            if (loanApproval == null)
            {
                return NotFound();
            }

            _context.LoanApproval.Remove(loanApproval);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool LoanApprovalExists(int id)
        {
            return _context.LoanApproval.Any(e => e.loan_approval_id == id);
        }
    }
}
