﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using credit_score_WebApi.Data;
using credit_score_WebApi.Models;

namespace credit_score_WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CreditReportsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public CreditReportsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/CreditReports
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CreditReport>>> GetCreditReports()
        {
            return await _context.CreditReports.ToListAsync();
        }

        // GET: api/CreditReports/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CreditReport>> GetCreditReport(int id)
        {
            var creditReport = await _context.CreditReports.FindAsync(id);

            if (creditReport == null)
            {
                return NotFound();
            }

            return creditReport;
        }

        // PUT: api/CreditReports/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCreditReport(int id, CreditReport creditReport)
        {
            if (id != creditReport.Id)
            {
                return BadRequest();
            }

            _context.Entry(creditReport).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CreditReportExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CreditReports
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<CreditReport>> PostCreditReport(CreditReport creditReport)
        {
            _context.CreditReports.Add(creditReport);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCreditReport", new { id = creditReport.Id }, creditReport);
        }

        // DELETE: api/CreditReports/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCreditReport(int id)
        {
            var creditReport = await _context.CreditReports.FindAsync(id);
            if (creditReport == null)
            {
                return NotFound();
            }

            _context.CreditReports.Remove(creditReport);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CreditReportExists(int id)
        {
            return _context.CreditReports.Any(e => e.Id == id);
        }
    }
}
