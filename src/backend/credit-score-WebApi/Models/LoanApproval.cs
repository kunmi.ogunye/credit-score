﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace credit_score_WebApi.Models
{
    public class LoanApproval
    {
        [Key]
        public int loan_approval_id { get; set; }

        [ForeignKey(nameof(LoanApplication))]
        public int loan_application_id { get; set; }

        [ForeignKey(nameof(LoanEligibility))]
        public int loan_eligibility_id { get; set; }
    }
}
