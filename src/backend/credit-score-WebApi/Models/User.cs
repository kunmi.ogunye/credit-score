﻿using System.ComponentModel.DataAnnotations;

namespace credit_score_WebApi.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string? email { get; set; }
        public string? password { get; set; }
        public string? first_name { get; set; }
        public string? last_name { get;set; }
        public string? address { get; set; }
        public string? nin { get; set; }
    }
}
