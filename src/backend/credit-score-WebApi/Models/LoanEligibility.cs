﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace credit_score_WebApi.Models
{
    public class LoanEligibility
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey(nameof(CreditReport))]
        public int credit_report_id { get; set; }
        public decimal max_loan_amount { get; set; }
        public decimal interest_rate { get; set; }
        public int repayment_period { get; set; }
        public decimal monthly_repayment { get; set; }
    }
}
