﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace credit_score_WebApi.Models
{
    public class CreditReport
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey(nameof(User))]
        public int user_id { get; set; }
        public string? credit_history { get; set; }
        public decimal debts { get; set; }
    }
}
