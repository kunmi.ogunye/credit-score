﻿using credit_score_Application.BaseContracts;
using credit_score_Infrastructure.Service;
using credit_score_Infrastructure_Repository;
using credit_score_Infrastructure_Repository.Contexts;
using credit_score_Infrastrucutre.ServiceContract;
using credit_score_WebApi.Data;
using LoggerService;
using Microsoft.EntityFrameworkCore;

namespace credit_score_WebApi.Extensions
{
    public static  class ServiceRegistration
    {
        public static void ConfigureCors(this IServiceCollection services) => services.AddCors(opt =>
                                                                                       {
                                                                                           opt.AddPolicy("Policy", builder =>
                                                                                           builder.AllowAnyOrigin()
                                                                                           .AllowAnyMethod()
                                                                                           .AllowAnyHeader());
                                                                                       });


        public static void ConfigureIISIntegration(this IServiceCollection services) =>
            services.Configure<IISOptions>(option =>
            { });

        //Logger Service not yet implemented
        public static void ConfigureLoggerService(this IServiceCollection services) =>
            services.AddScoped<ILoggerManager, LoggerManager>();

        public static void ConfigureRepositoryManager(this IServiceCollection services) =>
            services.AddScoped<IRepositoryManager, RepositoryManager>();

        //created Configure Service Manager extension service for program.cs
        public static void ConfigureServiceManager(this IServiceCollection services) =>
            services.AddScoped<IServiceManager, ServiceManager>();

        public static void ConfigureSqlContext(this IServiceCollection services, IConfiguration configuration)
            => services.AddDbContext<RepositoryContext>(options =>
            options.UseSqlServer(configuration.GetConnectionString("sqlConnection")));
    }
}
