﻿using AutoMapper;
using credit_score_Domain.Models;
using credit_score_Infrastruture.Shareded.DTOs;

namespace credit_score_WebApi
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CreditReport, CreditReportDto>(); //,Getting things from the database
            CreateMap<CreditReportDto, CreditReport>(); // Sending things to the database
        }
    }
}
