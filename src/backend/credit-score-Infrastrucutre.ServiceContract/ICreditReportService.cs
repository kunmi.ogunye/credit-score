﻿using credit_score_Infrastruture.Shareded.DTOs;

namespace credit_score_Infrastrucutre.ServiceContract
{
    public interface ICreditReportService
    {
        Task<IEnumerable<CreditReportDto>> GetCreditReportsAsync(bool trackChanges);
        Task<CreditReportDto> GetCreditReportByIdAsync(int Id, bool trackChanges);
        Task<CreditReportDto> CreateCreditReportAsync(CreditReportForCreationDto forCreationDto);
        Task UpdateCreditReportAsync(int Id, CreditReportForUpdate forUpdate, bool trackChanges);
    }
}
