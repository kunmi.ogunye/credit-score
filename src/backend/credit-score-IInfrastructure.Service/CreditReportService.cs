﻿using AutoMapper;
using credit_score_Application.BaseContracts;
using credit_score_Domain.Models;
using credit_score_Infrastrucutre.ServiceContract;
using credit_score_Infrastruture.Shareded.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace credit_score_Infrastructure.Service
{
    internal sealed class CreditReportService : ICreditReportService
    {
        private readonly IRepositoryManager _repository;
        private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;

        public CreditReportService(IRepositoryManager repository,ILoggerManager logger, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<CreditReportDto> CreateCreditReportAsync(CreditReportForCreationDto forCreationDto)
        {
            var creditReportEntity = _mapper.Map<CreditReport>(forCreationDto);
            _repository.CreditReport.CreateCreditReport(creditReportEntity);

            await _repository.SaveAsync();

            var entityToreturn = _mapper.Map<CreditReportDto>(creditReportEntity);
            return entityToreturn;
        }

        public async Task<CreditReportDto> GetCreditReportByIdAsync(int Id, bool trackChanges)
        {
            var getCreditReportEntity = await _repository.CreditReport.GetCreditReportById(Id, trackChanges);

            var creditReportToReturn  = _mapper.Map<CreditReportDto>(getCreditReportEntity);

            _logger.LogInfo($"Request for credit report single instance with Id:{getCreditReportEntity.Id}");

            return creditReportToReturn;
        }

        public async Task<IEnumerable<CreditReportDto>> GetCreditReportsAsync(bool trackChanges)
        {
            //Exception Handling goes here
            var creditReportEntity = await _repository.CreditReport.GetAllCreditReports(trackChanges);
            //Debug in code below "CreditReportService" is wrong. 
            //var creditReportToReturn = _mapper.Map<IEnumerable<CreditReportService>>(creditReportEntity);
            //return (IEnumerable<CreditReportDto>)creditReportToReturn;

            var creditReportToReturn = _mapper.Map<IEnumerable<CreditReportDto>>(creditReportEntity);
            return creditReportToReturn;
        }

        public async Task UpdateCreditReportAsync(int Id, CreditReportForUpdate forUpdate, bool trackChanges)
        {
            var creditEntity = await _repository.CreditReport.GetCreditReportById(Id, trackChanges);
            //Global Exception Handling 
            //if(creditEntity is null) 
             //   throw new CreditReportNotFoundException(Id);

            _mapper.Map(forUpdate, creditEntity);
            await _repository.SaveAsync();
        }
    }
}
