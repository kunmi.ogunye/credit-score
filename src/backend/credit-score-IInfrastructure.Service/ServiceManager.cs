﻿using AutoMapper;
using credit_score_Application.BaseContracts;
using credit_score_Infrastrucutre.ServiceContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace credit_score_Infrastructure.Service
{
    public sealed class ServiceManager : IServiceManager
    {
        private readonly Lazy<ICreditReportService> _creditReportService;

        public ServiceManager(IRepositoryManager repositoryManager, ILoggerManager logger, IMapper mapper)
        {
            _creditReportService = new Lazy<ICreditReportService>(() => 
            new CreditReportService(repositoryManager, logger, mapper));
        }
        public ICreditReportService CreditReportService => _creditReportService.Value;
    }
}
