﻿using credit_score_Domain.Models;

namespace credit_score_Application.Interfaces
{
    public interface ICreditReportRepository
    {
        Task<IEnumerable<CreditReport>> GetAllCreditReports(bool trackChanges);
        Task<CreditReport> GetCreditReportById(int id, bool trackChanges);
        void CreateCreditReport(CreditReport creditReport);
        void DeleteCreditReport(CreditReport creditReport);  
    }
}
