﻿using credit_score_Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace credit_score_Application.BaseContracts
{
    public interface IRepositoryManager
    {
        ICreditReportRepository CreditReport {  get; }
        Task SaveAsync();
    }
}
