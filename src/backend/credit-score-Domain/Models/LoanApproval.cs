﻿using credit_score_Domain.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace credit_score_Domain.Models
{
    public class LoanApproval : AduitableBaseEntity
    {        

        [ForeignKey(nameof(LoanApplication))]
        public int loan_application_id { get; set; }

        [ForeignKey(nameof(LoanEligibility))]
        public int loan_eligibility_id { get; set; }
    }
}
