﻿using credit_score_Domain.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.AccessControl;

namespace credit_score_Domain.Models
{
    public class CreditReport : AduitableBaseEntity
    {

        [ForeignKey(nameof(User))]
        public int user_id { get; set; }
        public string? credit_history { get; set; }
        public decimal debts { get; set; }
    }
}
