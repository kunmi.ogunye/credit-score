﻿using credit_score_Domain.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace credit_score_Domain.Models
{
    public class LoanEligibility : AduitableBaseEntity 
    {
        
        [ForeignKey(nameof(CreditReport))]
        public int credit_report_id { get; set; }
        public decimal max_loan_amount { get; set; }
        public decimal interest_rate { get; set; }
        public int repayment_period { get; set; }
        public decimal monthly_repayment { get; set; }
    }
}
