﻿using credit_score_Domain.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace credit_score_Domain.Models
{
    public class LoanApplication : AduitableBaseEntity
    {
       
        [ForeignKey(nameof(User))]
        public int user_id { get; set; }
        public decimal loan_amount { get; set; }
        public string? purpose { get; set; }
        public string? status { get; set; }
    }
}
